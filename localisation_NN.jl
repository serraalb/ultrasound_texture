include("nn_utilities.jl")
include("utilities.jl")
#using ImageDraw
using Conda
using PyCall
Conda.add("scikit-image")
find_boundaries = pyimport("skimage.segmentation").find_boundaries


"""
The path to positives and negative sapmles in equal proportions
with w width and h =length
"""


w=8; h=55;
base_path=raw"C:\Users\kaushik\Desktop\Ultrasound_project_articles\data_majtan\corrected_polygons/"
positives=joinpath(base_path, "ovr_equal_pos_80\\")
negatives=joinpath(base_path, "ovr_equal_neg_80\\")
#file_pos = readdir(positives)
#file_neg = readdir(negatives)

   

"""
The data contains both positives and negative samples in equal proportions
ImagesDataset provides this data

"""

data=ImagesDataset(positives, negatives)

train, test = splitobs(data, at=.7)
#MLUtils.getobs(data ,100)[2], MLUtils.getobs(train.data ,100)[2]

@show length(data) length(train) length(test)

"""
for (i, (instance, label)) in enumerate(MLUtils.DataLoader(train, 4))
    @info i instance label
end

for (train_data, val_data) in MLUtils.kfolds(data; k=10)
    for epoch = 1:1
        # Iterate over the data using mini-batches of 5 observations each
        for (x, y) in MLUtils.eachobs(train_data, batchsize=5)
            # ... train supervised model on minibatches here
            println(x,y)
        end
    end
end
"""
X_train_raw = zeros(h,w,length(train)); #(h, w, length)
y_train_raw=Int.(zeros(length(train)))

X_test_raw= zeros(h,w,length(test));  #(h, w, length)
y_test_raw=Int.(zeros(length(test)))



# Fill the arrays

for i =1: length(train)
    #println(i)
    X_train_raw[:,:, i] = Float64.(MLUtils.getobs(train ,i)[1])
    y_train_raw[i] = MLUtils.getobs(train ,i)[2];
end

for i=1: length(test)
    #println(i, size(Float64.(MLUtils.getobs(test ,i)[1]);))
    X_test_raw[:,:, i] .= Float64.(MLUtils.getobs(test ,i)[1]);
    y_test_raw[i] = MLUtils.getobs(test ,i)[2];
end

#img = X_test_raw[:, :, index]
#colorview(Gray, img')

# flatten input data for NN
X_train = Flux.flatten(X_train_raw)
X_test = Flux.flatten(X_test_raw)


# one-hot encode labels

y_train = onehotbatch(y_train_raw, 0:1)
y_test = onehotbatch(y_test_raw, 0:1)

# define model architecture
model=model_1
# define loss function
loss(x, y) = crossentropy(model(x), y)

# track parameters

ps = Flux.params(model)

# select optimizer
learning_rate = 0.001
opt = ADAM(learning_rate)

# train model
loss_history = []
epochs = 600

for epoch in 1:epochs
    # train model
    train!(loss, ps, [(X_train, y_train)], opt)
    # print report
    train_loss = loss(X_train, y_train)
    push!(loss_history, train_loss)
    println("Epoch = $epoch : Training Loss = $train_loss")
end

#Flux.@epochs 50 train!(loss, ps, [(X_train, y_train)], opt, cb = () -> println("training and loss=", loss(X_train, y_train)))
# make predictions

y_hat_raw = model(X_test)
y_hat = onecold(y_hat_raw) .- 1
y = y_test_raw

println("The accuracy= ", mean(y_hat .== y));

# display results

#check = [y_hat[i] == y[i] for i in 1:length(y)]
#index = collect(1:length(y))
#check_display = [index y_hat y check]
cm=ConfusionMatrix(y, y_hat);
println("Precision=", precision(cm));
println("Recall=", recall(cm));

#X_test_mat=reshape(X_test[:,:], (55,8,:))
"""
Get the images for testing. The test image is 
    cropped into wxh sized tiles
"""

path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//"
all_images =prepare_data_classification(path);
#colorview(Gray,all_images[index].img)
#index=8;# for image 8,234,59,152, 127, 81,56, 47, 145
for index=1: length(all_images)
 
    ref=similar(all_images[index].img);
    ref=all_images[index].img;
    ref_mask=all_images[index].full_mask;
    test_img=similar(all_images[index].img);
    test_img .= all_images[index].img;
    tiles, tiles_axis=image_to_tiles(test_img , w,h)

    for indx = 1: length(tiles)# for the tile in an image
    #indx=15;
            tile_flat = Flux.reshape(tiles[indx], length(tiles[indx]))
            a=tiles_axis[indx]
        if ( (a[1][end]-a[1][1])==(h-1)  && (a[2][end]-a[2][1])==(w-1))
            if (model(tile_flat)[1]> 0.5) # the tile belongs to negative class
            test_img[tiles_axis[indx][1],tiles_axis[indx][2]].=0
            end
        end 
    end

#p=Polygon([Point(x[1,i],y[2,i]) for i=1:n])
#p=Polygon(inds) 
#display(draw!(test_img, p, colorant"yellow"))

    border = find_boundaries(ref_mask)
    saveimg=hcat(test_img, ref .+ border)
    #imshow(saveimg)
    save(path*"//report_figs//ovr_55_8_NN_80//$(index).png",   Gray.(map(clamp01nan, saveimg)))
end

ImageView.closeall()
