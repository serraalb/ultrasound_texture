using Colors: Linear3
using MLJ: MLJBase
using Printf: decode
using StaticArrays: zeros, maximum, reshape
using Base: Order
using MLJBase: MLJModelInterface
using StatsBase
using Plots
using StatsPlots
using LinearAlgebra
using MLJ, MLJBase
import LIBSVM
import HypothesisTests

include("utilities.jl")
include("wavelet.jl")
include("roc_new.jl")
include("models.jl")
gr()

#decision.values = TRUE
textures=[]
#path="C://Users//HP//Desktop//HyperTension//corrected//"
path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//old//"


if !isdir(path)
    path="/mnt/medical/Ultrasound/majtan/"
    if !isdir(path)
        error("Could not set up the directory root")
    end
end
#path="C://Users//HP//Desktop//HyperTension//Repo_with_dots//"
textures =prepare_data_classification(path);

# To split the data into test and training sets
n=length(textures)
#textures = textures[randperm(n)];
maxlevel=2; h=3*maxlevel+1 # For Wavelet features
h=7; # For [mean, variance]
#h=175;# For [haralick_features]

X=zeros(n, h);
y=zeros(Int,n);
#n=168
for i = 1:n
     
    img=textures[i].patch; 
    mask=textures[i].masked; 
    
    #features1= haralick_features(img, mask)
    #features3=[mean(img); var(img)]; # [mean, var]
    
      
    features2=waveletdescr(img, mask, maxlevel)
    #features2=features2/length(img)
    
    #features=var(features);#[mean, var + features]
     
    X[i,:] .= features2; 
    y[i]=textures[i].class;
   
end   

#X=X[:,randperm(h)]


means= Statistics.mean(X,dims=1)
X = (X .- means) ./ Statistics.std(X, mean=means, dims=1)

inds=goodFeatures(X, y, 10);
X=X[:,inds]

# Preparation for The t- test, arranging the features and samples from each group
# N= (30):total number of samples taken from each of the populations (e.g C-EH, C-PHA, EH, PHA)
c1=0;c2=0;c3=0; #N=70;
#nf=size(X,2); # nf: no of features
fC=zeros(91, nf); fEH=zeros(50, nf); fPH=zeros(118, nf); #old
#fC=zeros(91, nf); fEH=zeros(185, nf); fPH=zeros(255, nf); #both

meanC=zeros(91); meanEH=zeros(50); meanPH=zeros(118); 

for i = 1 : n
    
    if ( (textures[i].class== 1)  )
        c1=c1+1;
        fC[c1,:] .= X[i,:];
        meanC[c1] += mean(textures[i].patch.*textures[i].masked)
    end
    
    
    if ( (textures[i].class== 2) )
        c2=c2+1;
        fEH[c2,:] .= X[i,:];
        meanEH[c2] += mean(textures[i].img.*textures[i].masked)
    end
    if ( (textures[i].class== 3) )
        c3=c3+1;
        fPH[c3,:] .= X[i,:];
        meanPH[c3] += mean(textures[i].img.*textures[i].masked)
    end
end

[mean(fPH[:,1]) mean(fPH[:,2]) mean(fPH[:,3]) mean(fPH[:,4]) mean(fPH[:,5]) mean(fPH[:,6]) mean(fPH[:,7])]
[std(fPH[:,1]) std(fPH[:,2]) std(fPH[:,3]) std(fPH[:,4]) std(fPH[:,5]) std(fPH[:,6]) std(fPH[:,7])]

[mean(fEH[:,1]) mean(fEH[:,2]) mean(fEH[:,3]) mean(fEH[:,4]) mean(fEH[:,5]) mean(fEH[:,6]) mean(fEH[:,7])]
[std(fEH[:,1]) std(fEH[:,2]) std(fEH[:,3]) std(fEH[:,4]) std(fEH[:,5]) std(fEH[:,6]) std(fEH[:,7])]

[mean(fC[:,1]) mean(fC[:,2]) mean(fEH[:,3]) mean(fC[:,4]) mean(fC[:,5]) mean(fC[:,6]) mean(fC[:,7])]
[std(fC[:,1]) std(fC[:,2]) std(fC[:,3]) std(fC[:,4]) std(fC[:,5]) std(fC[:,6]) std(fC[:,7])]


"""
# To draw pdfs for C EH and PHA
c1=1;c2=1;c3=1; nf=size(X,2); # nf: no of features
fC=zeros(91, nf); fEH=zeros(49, nf); fPH=zeros(117, nf);
for i = 1 : n
    if ( (textures[i].class== 1) )# (N-1) )
         fC[c1,:] .= X[i,:];
         c1=c1+1;
    elseif ( (textures[i].class== 2) )#(N-1) )
          fEH[c2,:] .= X[i,:];
          c2=c2+1;
    elseif ( (textures[i].class== 3) )#(N-1) )
          fPH[c3,:] .= X[i,:];
          c3=c3+1;
    end
end
"""

#NO=257;
nf=7
n1=Distributions.fit(Normal,fPH[:,1:nf] )
x = sort!(rand(Normal(n1.μ, n1.σ), n))
y1 = pdf(Normal(n1.μ, n1.σ) , x) #even with just pdf(d,a)
(plot(x,y1, label="PH"))

n2=Distributions.fit(Normal,fEH[:,1:nf] )

x = sort!(rand(Normal(n2.μ, n2.σ), n))
y1 = pdf(Normal(n2.μ, n2.σ), x) 
(plot!(x, y1, label="EH"))

n3=Distributions.fit(Normal,fC[:,1:nf] )
x = sort!(rand(Normal(n3.μ, n3.σ), n))
y1 = pdf(Normal(n3.μ, n3.σ) , x) 
plot!(x,y1, ylabel="Number of samples in three classes", label="Controls" ) 

"""
Box plots
"""
for f=1: nf
    feature=[fC[1:50,f] fEH[1:50,f] fPH[1:50,f] ]
    p=boxplot(["Controls" "EH" "PA" ], feature , ylabel="Wavelet feature", xlabel="Classes", leg = false)
    display(p)
    savefig(p, "C://Users//kaushik//Desktop//Dr Holaj_Paper//box_plots//$(f).pdf")

end


# Histogram

df = DataFrame([fC[1:50,f] fEH[1:50,f] fPH[1:50,f]], :auto);
df=[]
for f=1: nf
push!(df,DataFrame([fC[1:50,f] fEH[1:50,f] fPH[1:50,f]], :auto);)
#display(histogram([fC[1:50,f], fEH[1:50,f] ,fPH[1:50,f]], fillcolor=[:red :green :blue],label=["Controls" "EH" "PA"]))
end
for f=1: nf
 #readline()
 plot(histogram.(eachcol(df[f]))..., fillcolor=[:red :green :blue], label=["Controls" "EH" "PA"] )
end
"""
To compute the mean and stddev for the features in individual groups
"""
 # set the number of feature for which mean and std_dev is needed
 nf=7
for i=1: nf
    println("");
   print("The mean for Controls, feature $i= ", mean(fC[:,i])); println(" and std=", sqrt(var(fC[:,i])));
   print("The mean for EH, feature $i= ",mean(fEH[:,i])); println(" and std=", sqrt(var(fEH[:,i])));
   print("The mean for PH, feature $i= ",mean(fPH[:,i])); println(" and std=", sqrt(var(fPH[:,i])));

end


# Compute t value for two samples (three pairs: C-EH, C-PHA, EH_PHA)
t_C_EH=zeros(nf);t_C_PH=zeros(nf);t_EH_PH=zeros(nf);
p_C_EH=zeros(nf);p_C_PH=zeros(nf);p_EH_PH=zeros(nf);

for f= 1: nf
t_C_EH[f], p_C_EH[f] =TwoSampleT2Test(fC[:,f:f] ,fEH[:,f:f]);
t_C_PH[f], p_C_PH[f]=TwoSampleT2Test(fC[:,f:f] , fPH[:,f:f]);
t_EH_PH[f],p_EH_PH[f]=TwoSampleT2Test(fEH[:,f:f] ,fPH[:,f:f]);
end

#p_EH_PH[:]'


"""
From HypothesisTests package between Controls and PA
"""
welchp=zeros(nf);mwp=zeros(nf);ksp=zeros(nf)
for f= 1: nf
    #f1=f+142
    f1=f
welchp[f]=HypothesisTests.pvalue(HypothesisTests.UnequalVarianceTTest(fPH[:,f1] , fEH[:,f1]),tail=:both)
ksp[f]=HypothesisTests.pvalue(HypothesisTests.ApproximateTwoSampleKSTest(fPH[:,f1] , fEH[:,f1]),tail=:both)
mwp[f]=HypothesisTests.pvalue(HypothesisTests.MannWhitneyUTest(fPH[:,f1] , fEH[:,f1]),tail=:both)
end
    

    

"""
This is TSquared test for multiple variables p-values

t_C_EH, p_C_EH =TwoSampleT2Test(fC[:,1:nf] ,fEH[:,1:nf]);
t_C_PH, p_C_PH=TwoSampleT2Test(fC[:,1:nf] ,fPH[:,1:nf]);
t_EH_PH,p_EH_PH=TwoSampleT2Test(fEH[:,1:nf] ,fPH[:,1:nf]);
"""