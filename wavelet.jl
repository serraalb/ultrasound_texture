using Statistics


"""
The implementation is taken from:
SVOBODA, Tomas; KYBIC, Jan; HLAVAC, Vaclav.
Image processing, analysis & and machine vision-a MATLAB companion.
Thomson Learning, 2007.
"""

function filterh(img, l)
    img = 0.5 .* [img; img[(end-1):-1:(end-l), :]]
    return img[1:end-l, :] + img[(l+1):end, :]
end

function filterg(img, l)
    img = 0.5 .* [img; img[(end-1):-1:(end-l), :]]
    return img[(l+1):end, :] - img[1:end-l, :]
end

"""
```
v = waveletdescr(img, maxlevel)
```

Compute a vector of texture descriptors of size `3 * maxlevel + 1` from the given
image `img`.
"""
function descr(img, mask)
    # dim mismatch problem!    
    #descr(x) = sum(x .^ 2) / length(x)
    #inds=findall( ==(true) , mask)

    #return sum(img[inds].^2) / sum(mask)
    return sum(( img .* mask).^2) / sum(mask)
end


function waveletdescr(img, mask, maxlevel::Int)
     
    v = zeros(3 * maxlevel + 1)
    for i in 1:maxlevel
        l = 2^i
        imhy = filterh(img, l)
        imgy = filterg(img, l)
        j=(3 * i - 2):(3 * i)
        #@info j
        v[(3 * i - 2):(3 * i)] .= [descr(filterg(imgy', l)', mask),
                                   descr(filterh(imgy', l)', mask),
                                   descr(filterg(imhy', l)', mask) ]
        img = filterh(imhy', l)'
    end
    #@info 7
    v[end] = descr(img, mask)
      
     
    return v
end

