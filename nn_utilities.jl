# load packages
using Flux, Images, MLDatasets, Plots
using Flux: crossentropy, onecold, onehotbatch, train!
using LinearAlgebra, Random, Statistics
#using DataLoaders
using Random
using EvalMetrics

#import DataLoaders: DataLoader
#import DataLoaders.LearnBase: getobs, nobs
import MLUtils
import MLUtils: splitobs
import Revise
using ImageIO


struct ImagesDataset
    files::Vector{String}
    labels::Vector{Int}
end


function ImagesDataset(positivedir::AbstractString, negativedir::AbstractString)
    
    negatives = readdir(negativedir, join=true)
    positives = readdir(positivedir, join=true)

    files = [negatives; positives ]
    labels = [zeros(length(negatives)); ones(length(positives)) ]

    # Shuffle
    ix = randperm(length(files))
    ImagesDataset(files[ix], labels[ix])
end

Base.length(d::ImagesDataset) = length(d.files)
MLUtils.getobs(d::ImagesDataset, i) = ( Gray.(Images.load(d.files[i])), d.labels[i])

"""
model = Chain(
    Dense(8*55, 32, relu),
    Dense(32, 2),
    softmax
)
"""

model_3 = Chain(
    Dense(8*55, 32, relu),
    Dense(32, 2),
    softmax
)

# 440 for 55x8 and 784 for 28x28 and 
# 319 for 29x11
model_1 = Chain(
    Dense(440, 100, relu),
    Dense(100, 10, relu),
    Dense(10, 2),
    softmax
);

model_2 = Chain(
    # First convolution, operating upon a 28x28 image
    Conv((3, 3), 1=>16, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Second convolution, operating upon a 14x14 image
    Conv((3, 3), 16=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Third convolution, operating upon a 7x7 image
    Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Reshape 3d tensor into a 2d one, at this point it should be (3, 3, 32, N)
    # which is where we get the 288 in the `Dense` layer below:
    x -> reshape(x, :, size(x, 4)),
    Dense(288, 10),
    Dense(10, 2),

    # Finally, softmax to get nice probabilities
    softmax
);

