using StatsBase
using  FileIO
using Plots
using LinearAlgebra
using DelimitedFiles
using ImageDraw
using Images, ImageView


srcpath="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//PA_new//new_PA_images_masks//"


im_path=srcpath*"images//"
mask_path=srcpath*"masks//"
dstpath="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//new//PA//"
samples=readdir(mask_path)

count=0;

for file= 1: length(samples)
   
     #p=[]
   #println(file)
        path_img=joinpath(mask_path,"$(file).bmp")
        img=Images.load(path_img);
        labels=label_components(img, trues(7,7), -1)
        cb=component_boxes(labels')
        no_masks=maximum(labels)
        for i=2:no_masks

            mask = labels .!= i
            mask= @. !(mask)
            """
            r=cb[1+i]
            p=Polygon([Point(r[1][1], r[1][2]), Point(r[2][1], r[1][2]), 
                   Point(r[2][1], r[2][2]), Point(r[1][1],r[2][2])])

            cmask=convert.(RGB, mask)
            draw!(cmask, p, colorant"red")
            imshow(cmask)
            short_mask=mask[r[1][2]:r[2][2], r[1][1]:r[2][1] ]
            """
            count=count +1
            orig_img=Images.load(joinpath(im_path,"$(file).bmp"));
            save(dstpath*"images//"*"$(count).bmp", orig_img)
            save(dstpath*"masks//$(count).bmp", mask)   
        

        end
    end

        
        
dstpath="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//new//EH//images//"
        
samples=readdir(dstpath)

count=0;

for file =1: length(samples)
    
       count=count +1 
       path_img=joinpath(dstpath,"$(samples[file])")
       img=Images.load(path_img)
       save(dstpath*"$(count).bmp", mask) 
       println(count)
end

               
           
        

        