using MLJ 
using MLJBase
import LIBSVM
using MLJBase: MLJModelInterface

function Fit( X, y, train)
     yint = MLJBase.int(y)
     a_target_element = y[1]                    # a CategoricalValue/String
     d = MLJBase.decoder(a_target_element) # can be called on integers
 
     core_fitresult =   MLJ.fit!(svc_machine, rows=train, force=true)  
 
     fitresult = (d, core_fitresult)
     cache = nothing
     report = nothing
     return fitresult, cache, report
 end

 function Predict( fitresult, Xnew)
     d, core_fitresult = fitresult
     
     yhat = MLJ.predict(core_fitresult, rows=test)
     return d.(yhat)  # or decode(yhat) also works
 end

 #CategoricalArrays.CategoricalValue{Int64,UInt32} 1 (1/2)


function svm_model(X,y, r1::NumericRange{Float64,MLJBase.Bounded,Symbol}, 
                        r2::NumericRange{Float64, MLJBase.Bounded,Symbol})
  
n=length(y); 

svm = @load SVC() pkg=LIBSVM 
svmm=svm()# 72

#pipe = @pipeline(Standardizer, OneHotEncoder,  svmm)

#r1= range(svmm, scale=:log, :gamma, lower=2^-5, upper=2^7);#lower=0.001, upper=1.0
#r2= range(svmm, scale=:log, :cost, lower=2^-4, upper=2^15);

stratified_cv = StratifiedCV(; nfolds=5, shuffle=true)
svc_tuned = TunedModel(model=svmm, measure=MisclassificationRate(),
                 tuning = Grid(resolution=20), 
                  resampling=stratified_cv,                        
                  range=[r1, r2]);

#w=ones(length(y))
#w[y.==1] .= 2.0

"""
maxim=Statistics.maximum(X,dims=1);minm=Statistics.minimum(X,dims=1);
#X1 = (X .- minm) ./ (maxim .- minm)

means= Statistics.mean(X,dims=1)
X1 = (X .- means) ./ Statistics.std(X, mean=means, dims=1)
"""
X1 = MLJ.table(X);
y1 = categorical(y, ordered=true);


svc_mach = machine(svc_tuned, X1, y1);
#ypred=decoder.(yhat)
svc_mach.model
fit!(svc_mach)
#report(svc_mach)[1]
fitted_svc=fitted_params(svc_mach).best_model
#fitted_svc.cost=1.0
#fitted_svc.gamma=0.10
#svc_machine = machine(svc, X, y);
# int turn it to int and d is inv of int
#fitted_svc=0
 # Cross Validation with kfolds=4
     xx=[]; yy=[]; nfolds=10; loop=0;N=1;
     precision_svm=zeros(nfolds); accuracy_svm =zeros(nfolds); 
     recall_svm=zeros(nfolds);   
     specificity_svm=zeros(nfolds);
     
     cv=CV(nfolds=10, shuffle=true);
     t= MLJBase.train_test_pairs(cv, 1:n)

# This loop is to find averages of accuracy, precision and recall over k folds


for fold=1:nfolds
      loop=loop+1;
     
     
     train=t[fold][1]; test=t[fold][2];
     
     Xtrain=X[train,:];
     
     """
     maxim=Statistics.maximum(Xtrain,dims=1);minm=Statistics.minimum(Xtrain,dims=1);
     Xtrain = (Xtrain .- minm) ./ (maxim .- minm)
      """  
     means= Statistics.mean(X,dims=1)
     Xtrain = (Xtrain .- means) ./ Statistics.std(Xtrain, mean=means, dims=1)

     
    
     model=LIBSVM.svmtrain( Xtrain', y[train],
                                    gamma=fitted_svc.gamma,
                                   cost=fitted_svc.cost, kernel=LIBSVM.Kernel.RadialBasis)
                                   #probability=fitted_svc.probability=true)#,  probability=fitted_svc.probability=true )
     
     Xtest=X[test,:];  
     
     Xtest = (Xtest .- means) ./ Statistics.std(Xtest, mean=means, dims=1)                            
     """
     Xtest = (Xtest .- minm) ./ (maxim .- minm)
     """
     svm_preds, dec_vals_svm = LIBSVM.svmpredict(model, Xtest')#probability=fitted_svc.probability=true
     
     #y_test= Int32.(int.(y[test])) .- 1 ;
     #y_test= Bool.(y_test);

     dec_vals_svm1 = dec_vals_svm[1, :]
     
    if dec_vals_svm1[1] > 0.0
         y_test = y[test] .== svm_preds[1]
    elseif (dec_vals_svm1[1] <= 0.0)
         y_test = y[test] .!= svm_preds[1]
    end
    """
    v1 = dec_vals_svm1[y[test] .== 1]
    v2 = dec_vals_svm1[y[test] .== 2]
    histogram(v1, bins=100)
    histogram!(v2, bins=100)
   """
    m = length(y_test)
    P = sum(y_test)
    N = m - P
    tprs_svm, fprs_svm, wp_ix_svm= roc_curve(dec_vals_svm1, y_test);
    #auc_roc_svm = roc_auc(tprs_svm, fprs_svm)
    ind_svm=argmax(sqrt.(tprs_svm .* (1 .- fprs_svm)))
    TPR_thresh=tprs_svm[ind_svm]; FPR_thresh=tprs_svm[ind_svm];

    push!(xx,tprs_svm); push!(yy,fprs_svm);
    tp_thresh=P*TPR_thresh; fp_thresh=N*FPR_thresh ;

     accuracy_svm[fold] = 1- misclassification_rate(categorical(svm_preds), y1[test])
     precision_svm[fold] =tp_thresh/(tp_thresh + fp_thresh)
     recall_svm[fold] = TPR_thresh
     specificity_svm[fold]= 1- FPR_thresh
     

end


min_xx=minimum(size.(xx,1));
min_yy=minimum(size.(yy,1));
mean_xx=zeros(min_xx);mean_yy=zeros(min_yy)


for ii=1:nfolds
mean_xx .= mean_xx  .+  xx[ii][1:min_xx] ; 
mean_yy .= mean_yy  .+  yy[ii][1:min_yy] ;
end

avg_xx_svm=similar(mean_xx);avg_yy_svm=similar(mean_yy);
avg_xx_svm .=mean_xx ./ nfolds #maximum(mean_xx)
avg_yy_svm .=mean_yy ./ nfolds #maximum(mean_yy)

return fitted_svc,   avg_xx_svm, avg_yy_svm, xx, yy, accuracy_svm, 
        precision_svm, recall_svm, specificity_svm

end


# Random Forest Classifier

function  random_forest_model(X,y, r1::NumericRange{Int64,MLJBase.Bounded,Symbol}, 
                              r2::NumericRange{Int64,MLJBase.Bounded,Symbol},
                              r3::NumericRange{Int64,MLJBase.Bounded,Symbol})

n=length(y);
rfc =@load RandomForestClassifier pkg=DecisionTree 
rfcc=rfc()
stratified_cv = StratifiedCV(; nfolds=20, shuffle=true)
# Tuning RF
rf = TunedModel(model=rfcc,  resampling=stratified_cv,  
                     operation=predict_mode, tuning = Grid(resolution=10), 
                     range=[r1, r2, r3], measure=MisclassificationRate());
X1 = MLJ.table(X);
y1 = categorical(y, ordered=true);
RandomForest = machine( rf , X1, y1);
fit!(RandomForest) # It was on whole date before!!!(fit!(RandomForest)
#rep MLJ.fit!(RF, rows=train);ort(RandomForest)[1]

rf_tuned=fitted_params(RandomForest).best_model   
#RF = machine( rf_tuned , X, y); 



# Cross Validation with kfolds=4
xx=[];yy=[]; t_rf=[];nfolds=10;
precision_rf=zeros(nfolds); accuracy_rf=zeros(nfolds); recall_rf=zeros(nfolds);
specificity_rf=zeros(nfolds);

cv=CV(nfolds=10, shuffle=true);
s_cv = StratifiedCV(; nfolds=10, shuffle=true)
t= MLJBase.train_test_pairs( cv, 1:n)


# This loop is to find averages of accuracy, precision and recall over k folds

     for fold=1:nfolds
                   
        
          train=t[fold][1]; test=t[fold][2];
          RF = machine(rf_tuned , X1, y1);
          MLJ.fit!(RF, rows=train);
          ypred_rf = MLJ.predict(RF, rows=test);

          fprs_rf, tprs_rf, ts_rf = MLJBase.roc_curve(ypred_rf, y1[test]) ;
          push!(xx,fprs_rf);push!(yy,tprs_rf); push!(t_rf,ts_rf)
          ypred_rf = mode.(ypred_rf);
          accuracy_rf[fold] = 1- misclassification_rate(ypred_rf, y1[test]); 
          precision_rf[fold] =1 - FalseDiscoveryRate()(ypred_rf, y1[test]);
          recall_rf[fold] = TruePositiveRate()(ypred_rf, y1[test]);
          specificity_rf[fold] = 1 - FalsePositiveRate()(ypred_rf, y1[test]);
         # auc_rf[fold]= AreaUnderCurve()(ypred_rf, y[test]);
          
     end


min_xx=minimum(size.(xx,1));
min_yy=minimum(size.(yy,1));
mean_xx=zeros(min_xx);mean_yy=zeros(min_yy)

     #=
     mean_xx .= xx[1][1:min_xx] .+  xx[2][1:min_xx] .+
                    xx[3][1:min_xx] .+  xx[4][1:min_xx] ; 
     mean_yy .= yy[1][1:min_yy] .+  yy[2][1:min_yy] .+
                    yy[3][1:min_yy] .+  yy[4][1:min_yy] ; 
     =#


for ii=1:nfolds
    mean_xx .= mean_xx  .+  xx[ii][1:min_xx] ; 
    mean_yy .= mean_yy  .+  yy[ii][1:min_yy] ;
end
                         
avg_xx_rf=similar(mean_xx);avg_yy_rf=similar(mean_yy);
avg_xx_rf=mean_xx ./ nfolds; #maximum(mean_xx) ;
avg_yy_rf=mean_yy ./ nfolds #maximum(mean_yy) ;

return  rf_tuned, avg_xx_rf, avg_yy_rf, xx, yy, accuracy_rf, precision_rf, recall_rf, specificity_rf, t_rf
    
end
