import Statistics

Base.@kwdef struct Scale
    shift::Vector{Float64}
    scale::Vector{Float64}
end

function minmax_scale(features::AbstractMatrix{Float64})
    shift = -1 .* minimum(features, dims=2)
    scale = 1 ./ (maximum(features, dims=2) .+ shift)
    return Scale(shift=shift[:], scale=scale[:])
end

function meanstd_scale(features::AbstractMatrix{Float64})
    μ = Statistics.mean(features, dims=2)
    σ = Statistics.std(features, mean=μ, dims=2)
    return Scale(shift=-μ[:], scale=(1 ./ σ)[:])
end

function (s::Scale)(features::AbstractMatrix{Float64})
    @assert length(s.shift) == size(features, 1)
    return (features .+ s.shift) .* s.scale
end
