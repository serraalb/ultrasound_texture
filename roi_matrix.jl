# This function overrides the checkbounds() in _glcm(). Using this function the checkbounds()
# accepts only the pixels inside irregular ROI region.
struct roiMatrix{T}<: AbstractArray{T, 2}
    img::Matrix{T}
    mask::Matrix{Bool}

    function roiMatrix(img::Matrix{T}, mask::Matrix{Bool}) where {T}
        size(img) == size(mask) || throw(DimensionMismatch())
        new{T}(img, mask)
    end
end
Base.size(S::roiMatrix) = size(S.img)
Base.getindex(S::roiMatrix, I...) = getindex(S.img, I...)
Base.checkbounds(::Type{Bool}, S::roiMatrix, i, j) = S.mask[i, j]


#=
img = rand(128, 128)
mask = rand(Bool, 128, 128)

S = roiMatrix(img, mask)
println(checkbounds(Bool, S, 15, 8))
=#
