include("utilities.jl")

"""
Provide the path of the main folder having three sub folders with samples
from each class
"""
path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//"

if !isdir(path)
    path="/mnt/medical/Ultrasound/majtan/"
    if !isdir(path)
        error("Could not set up the directory root")
    end
end

# all_images structure contains images from all three classes used for localisation
all_images =prepare_data_classification(path);

"""
This part of code stores all the images in folder /images (image_no_images)and the corresponding mask
of ROI with "nameno_mask" in separate folder /masks
"""

mkdir(path*"images_and_masks")
path=path*"images_and_masks"
path_images=path*"images"
path_masks=path*"masks"

if !isdir(path_images)
   mkdir(path_images);
end
if !isdir(path_masks)
    mkdir(path_masks);
end

for img_no = 1:size(all_images,1)

    save(path_images*"//$(img_no)_image.png", all_images[img_no].img) 
    save(path_masks*"//$(img_no)_mask.png", all_images[img_no].full_mask) 

end