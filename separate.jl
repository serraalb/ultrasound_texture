include("utilities.jl")
using Images

srcpath="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//PA_new//combined_PA//"
dstpath="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//PA_new//"

no_samples=length(readdir(srcpath));# The folder with minimum number of samples
dstpath=dstpath*"separate_PA"
if !isdir(dstpath)
    mkdir(dstpath);
end
    
samples=readdir(srcpath)


for file= 1: length(samples)
   
    path_bmp=joinpath(srcpath,"$(samples[file])")
    img=load(path_bmp);
   #println(file)
   if !(samples[file][end-4] == 'M')
        save(dstpath*"//$(samples[file])", img)
   end
end   