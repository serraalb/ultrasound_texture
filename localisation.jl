include("nn_utilities.jl")
include("utilities.jl")
#using ImageDraw
using Conda
using PyCall
Conda.add("scikit-image")
find_boundaries = pyimport("skimage.segmentation").find_boundaries


"""
The path to positives and negative sapmles in equal proportions
with w width and h =length
"""


w=28; h=28;
base_path=raw"C:\Users\kaushik\Desktop\Ultrasound_project_articles\data_majtan\corrected_polygons\total_samples_28"
positives=joinpath(base_path, "equal_pos\\")
negatives=joinpath(base_path, "equal_neg\\")
#file_pos = readdir(positives)
#file_neg = readdir(negatives)

   

"""
The data contains both positives and negative samples in equal proportions
ImagesDataset provides this data

"""

data=ImagesDataset(positives, negatives)

train, test = splitobs(data, at=.7)
#MLUtils.getobs(data ,100)[2], MLUtils.getobs(train.data ,100)[2]

@show length(data) length(train) length(test)

"""
for (i, (instance, label)) in enumerate(MLUtils.DataLoader(train, 4))
    @info i instance label
end

for (train_data, val_data) in MLUtils.kfolds(data; k=10)
    for epoch = 1:1
        # Iterate over the data using mini-batches of 5 observations each
        for (x, y) in MLUtils.eachobs(train_data, batchsize=5)
            # ... train supervised model on minibatches here
            println(x,y)
        end
    end
end
"""
X_train_raw = zeros(h,w,length(train)); #(h, w, length)
y_train_raw=Int.(zeros(length(train)))

X_test_raw= zeros(h,w,length(test));  #(h, w, length)
y_test_raw=Int.(zeros(length(test)))



# Fill the arrays

for i =1: length(train)
    println(i)
    X_train_raw[:,:, i] = Float64.(MLUtils.getobs(train ,i)[1])
    y_train_raw[i] = MLUtils.getobs(train ,i)[2];
end

for i=1: length(test)
    println(i, size(Float64.(MLUtils.getobs(test ,i)[1]);))
    X_test_raw[:,:, i] .= Float64.(MLUtils.getobs(test ,i)[1]);
    y_test_raw[i] = MLUtils.getobs(test ,i)[2];
end

# For CNN  model_2
X_train_raw=reshape(X_train_raw,(28,28,1,:))
X_test_raw=reshape(X_test_raw,(28,28,1,:))
X_train = X_train_raw
X_test = X_test_raw



#img = X_test_raw[:, :, index]
#colorview(Gray, img')

# flatten input data
#X_train = Flux.flatten(X_train_raw)
#X_test = Flux.flatten(X_test_raw)


# one-hot encode labels

y_train = onehotbatch(y_train_raw, 0:1)
y_test = onehotbatch(y_test_raw, 0:1)

# define model architecture
#w=8;h=55; #w*h=440
model_1 = Chain(
    Dense(28*28, 100, relu),
    Dense(100, 10, relu),
    Dense(10, 2),
    softmax
)

model_2 = Chain(
    # First convolution, operating upon a 28x28 image
    Conv((3, 3), 1=>16, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Second convolution, operating upon a 14x14 image
    Conv((3, 3), 16=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Third convolution, operating upon a 7x7 image
    Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Reshape 3d tensor into a 2d one, at this point it should be (3, 3, 32, N)
    # which is where we get the 288 in the `Dense` layer below:
    x -> reshape(x, :, size(x, 4)),
    Dense(288, 10),
    Dense(10, 2),

    # Finally, softmax to get nice probabilities
    softmax
)

# define loss function
loss(x, y) = crossentropy(model_2(x), y)

# track parameters

ps = Flux.params(model_2)

# select optimizer
learning_rate = 0.0001
opt = ADAM(learning_rate)

# train model
loss_history = []
epochs = 600

for epoch in 1:epochs
    # train model
    train!(loss, ps, [(X_train, y_train)], opt)
    # print report
    train_loss = loss(X_train, y_train)
    push!(loss_history, train_loss)
    println("Epoch = $epoch : Training Loss = $train_loss")
end

#Flux.@epochs 50 train!(loss, ps, [(X_train, y_train)], opt, cb = () -> println("training and loss=", loss(X_train, y_train)))
# make predictions

y_hat_raw = model_2(X_test)
y_hat = onecold(y_hat_raw) .- 1
y = y_test_raw

println("The accuracy= ", mean(y_hat .== y));

# display results

#check = [y_hat[i] == y[i] for i in 1:length(y)]
#index = collect(1:length(y))
#check_display = [index y_hat y check]
cm=ConfusionMatrix(y, y_hat);
println("Precision=", precision(cm));
println("Recall=", recall(cm));

#X_test_mat=reshape(X_test[:,:], (55,8,:))
"""
Get the images for testing. The test image is 
    cropped into wxh sized tiles
"""

path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//"
all_images =prepare_data_classification(path);
#colorview(Gray,all_images[index].img)
index=8;# for image 8,234,59,152, 127, 81,56, 47, 145
ref=all_images[index].img;
ref_mask=all_images[index].full_mask;
test_img=similar(all_images[index].img);
test_img .= all_images[index].img;
#test_img=X_test_raw[:,:, i];
colorview(Gray, test_img)
tiles, tiles_axis=image_to_tiles(test_img , w,h)

for indx = 1: length(tiles)# for the tile in an image
#indx=15;
    #tile_flat = Flux.reshape(tiles[indx], length(tiles[indx]))
      tile_flat=reshape(tiles[indx],(w,h,1,1))
      a=tiles_axis[indx]
    if ( (a[1][end]-a[1][1])==(w-1)  && (a[2][end]-a[2][1])==(h-1))
        if (model_2(tile_flat)[1]> 0.5) # the tile belongs to negative class
          test_img[tiles_axis[indx][1],tiles_axis[indx][2]].=0
        end
    end 
end



#p=Polygon([Point(x[1,i],y[2,i]) for i=1:n])
#p=Polygon(inds) 
#display(draw!(test_img, p, colorant"yellow"))

border = find_boundaries(ref_mask)
saveimg=hcat(test_img, ref .+ border)
imshow(saveimg)
save(path*"report_figs//f1.png",   Gray.(map(clamp01nan, saveimg)))
tmp=0; test_img=0;


ImageView.closeall()
