include("utilities.jl")
using StatsBase
"""
Provide the path to the folders containing neg_samples
and pos_samples. This scripts uniformly samples from these two folders in
equal_neg and equal_pos folders
"""
path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons"

srcpath=path*"ovr_neg_samples"
dstpath=path*"ovr_equal_neg"
src_rd=readdir(srcpath)
dst_rd=readdir(dstpath)

h=55; w=8;
no_samples=length(readdir(path*"ovr_pos_samples_80"));# The folder with minimum number of samples

samples=sample(readdir(path*"ovr_neg_samples_80"), no_samples, replace = false)
N=10894; # How many samples we need!
for file=1: N
    path_bmp=joinpath(srcpath*"//","$(samples[file])")
    img=load(path_bmp);

   if (size(img,1)==h && size(img,2)==w)
        save(dstpath*"//$(samples[file])", img)
   end
end   