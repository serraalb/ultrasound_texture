1.  The mutable structure "Feature" (in utilities.jl) is modified to store the names of the patients. 
2.  The function prepare_for_classification(path,type) accepts typr "new" or "old". The 
    The "path" contains two subfolders "new" and "old" each of these folder contains EA/images and  EA/masks
    PA/images and  PA/masks.
    The data is saved at:/mnt/medical/Ultrasound/majtan/final/new and ../final/old  
    This function populates the "Feature" structure with fields: class, image, mask, name
    The image and mask corresponds to cut of rectangular section of circumscribing the ROI
3.  The folder final/new/EH/ contains EH_distance.xlsx and final/new/PA/ contains PA_distance.xlsx
    These excel files contains name of the patients and distance in number of pixels per cm computed manually.
4.  The resizing of images and mask is done in classifier_ML.jl file. In the file textures_old and textures_new
    are computed ehich can be used separately or together for classification using SVM.
