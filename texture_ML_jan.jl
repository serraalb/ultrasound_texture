using MLJ: MLJBase
using Printf: decode
using StaticArrays: zeros, maximum, reshape
using Base: Order
using MLJBase: MLJModelInterface
using StatsBase
using Plots
using LinearAlgebra
using MLJ, MLJBase
import LIBSVM


include("utilities.jl")
include("wavelet.jl")
include("roc_new.jl")
include("models.jl")
gr()

#decision.values = TRUE
textures=[]
#path="C://Users//HP//Desktop//HyperTension//Repo_with_dots//"
#path="C://Users//HP//Desktop//HyperTension//corrected//"
path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//"

if !isdir(path)
    path="/mnt/medical/Ultrasound/majtan"
    if !isdir(path)
        error("Could not set up the directory root")
    end
end

    
textures =prepare_data_classification(path)

# To split the data into test and training sets
n=length(textures)
textures = textures[randperm(n)];

maxlevel=2; h=3*maxlevel+1 # Wavelet features
#h=2; # [mean, variance]
#h=175;# [haralick_features]
X=zeros(n, h);
y=zeros(Int,n);

for i = 1:n
     
    img=textures[i].patch; 
    mask=textures[i].masked; 
    
    #features2= haralick_features(img, mask)
    #features1=[mean(img); var(img)]; # [mean, var]
    
    #img=img.*mask;    
    features=waveletdescr(img, mask, maxlevel)
    
    #features=[features; features2];#[mean, var + features]
     
    X[i,:] .= features
    y[i]=textures[i].class;
   
end   

means= Statistics.mean(X,dims=1)
X = (X .- means) ./ Statistics.std(X, mean=means, dims=1)
#maxim=Statistics.maximum(X,dims=1);minm=Statistics.minimum(X,dims=1);
#X = (X .- minm) ./ (maxim .- minm)


inds=goodFeatures(X, y, 20);
X=X[:,inds]
#X=X[:, 6:20]



#..Support Vector machine
svm = @load SVC pkg=LIBSVM
svmm=svm()
gamma_range= range(svmm, scale=:log, :gamma, lower=2^-15, upper=2^17);#lower=0.001, upper=1.0
cost_range = range(svmm, scale=:log, :cost, lower=2^-4, upper=2^15);

#w_ix=0.
tuned_parameters_svm, avg_xx_svm, avg_yy_svm, xx, yy, accuracy_svm, 
            precision_svm, recall_svm, specificity_svm =  svm_model(X, y, gamma_range, cost_range);

display(tuned_parameters_svm)
ind_svm=argmax(sqrt.(avg_yy_svm .* (1 .- avg_xx_svm)))
scatter(avg_xx_svm[ind_svm:ind_svm], avg_yy_svm[ind_svm:ind_svm], markersize=8);
plot!(avg_xx_svm, avg_yy_svm, linewidth = 4, linecolor = :red, linestyle=[:solid], legend=false,size = (width=500, height=500) )
plot!(xx, yy, xlabel="False Positive Rate", ylabel="True Positive Rate", label="ROC for SVM",size = (width=500, height=500) )

#save("C:\\Users\\HP\\Desktop\\HyperTension\\paper\\f2\\sample_roc.png",p1)
#p1=plot(xx, yy, label="ROCs for SVM", xlabel="False Positive Rate",ylabel="True Positive Rate")
println("SVM Classifier accuracy: ", mean(accuracy_svm)*100);
println("SVM Classifier Standard Deviation: ", sqrt(var(accuracy_svm)) );
println("SVM Precision=", mean(precision_svm));print("SVM Recall=",mean(recall_svm))
println("SVM Specificity=", mean(specificity_svm));

     #=
     mean_xx .= xx[1][1:min_xx] .+  xx[2][1:min_xx] .+
                    xx[3][1:min_xx] .+  xx[4][1:min_xx] ; 
     mean_yy .= yy[1][1:min_yy] .+  yy[2][1:min_yy] .+
                    yy[3][1:min_yy] .+  yy[4][1:min_yy] ; 
     =#

#...Random Forest Classifier
rfc =@load RandomForestClassifier pkg=DecisionTree
rfcc=rfc()

ntrees_range = range(rfcc, :n_trees , lower=10, upper=50);
max_depth_range = range(rfcc, :max_depth, lower=4, upper=100);
min_samples_range= range(rfcc, :min_samples_leaf, lower=5, upper=50);

tuned_parameters_rf, avg_xx_rf, avg_yy_rf, xx, yy, accuracy_rf, 
precision_rf, recall_rf, specificity_rf, thresh_rf=random_forest_model(X, y, ntrees_range, max_depth_range, min_samples_range)

display(tuned_parameters_rf)

#plot!(xx, yy, xlabel="False Positive Rate", ylabel="True Positive Rate", la bel="ROC for Random Forest",size = (width=500, height=500) )

#t= mean.(thresh_rf);
ind_rf=argmax(sqrt.(avg_yy_rf .* (1 .- avg_xx_rf)));
scatter(avg_xx_rf[ind_rf:ind_rf], avg_yy_rf[ind_rf:ind_rf], markersize=5);

plot(avg_xx_rf, avg_yy_rf, linewidth = 4, linecolor = :green, linestyle=[:solid], legend=false,size = (width=500, height=500) )
plot!(xx, yy, xlabel="False Positive Rate", ylabel="True Positive Rate", label="ROC for Random Forest",size = (width=500, height=500) )

println("Sensitivity of RF Classifier at gm- threshold :", avg_yy_rf[ind_rf]);
println("Specificity of RF Classifier at gm- threshold :", 1 -avg_xx_rf[ind_rf])
println("RF Classifier accuracy: ", mean(accuracy_rf)*100);     println("RF Classifier Standard Deviation: ", sqrt(var(accuracy_rf)) );
println("RF Precision=", mean(precision_rf));print("RF Recall=",mean(recall_rf))
println("RF Specificity=", mean(specificity_rf)); #print("RF AUC=", mean(auc_rf))
# Plot the two ROC curves SVM and RF
scatter(avg_xx_rf[ind_rf:ind_rf], avg_yy_rf[ind_rf:ind_rf], markersize=5);
scatter!(avg_xx_svm[ind_svm:ind_svm], avg_yy_svm[ind_svm:ind_svm], markersize=8);
plot!(avg_xx_rf, avg_yy_rf, label= "RF", linewidth = 4, xlabel="False Positive Rate", ylabel="True Positive Rate",size = (width=500, height=500))
p2=plot!(avg_xx_svm, avg_yy_svm, label="SVM", linewidth = 4, xlabel="False Positive Rate", ylabel="True Positive Rate",legend=:bottomright,size = (width=500, height=500))

#save("C:\\Users\\HP\\Desktop\\HyperTension\\paper\\f3\\C_EH_f3_roc.png",p2)
