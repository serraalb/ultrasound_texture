using StatsBase
using  FileIO
using Plots
using LinearAlgebra
using DelimitedFiles
#using ImageDraw
using Images, ImageView
using LIBSVM
using Statistics


include("utilities.jl")
path="C://Users//HP//Desktop//HyperTension//"
textures, textures_train, textures_test=prepare_data_classification(path);

n=880# Haralick features
ntrain = length(textures_train)

#instances=Array{Float64}
instances=zeros(ntrain, n)
labels=zeros(ntrain)
#labels=Array{UInt8}


for i = 1:ntrain
     
      instances[i,:] .= haralick_features( textures_train[i].patch )
      labels[i]=textures_train[i].class
     
     
end


ntest = length(textures_test)
test_instances=zeros(ntest, n)
test_labels=zeros(ntest)

for i = 1:ntest
     
      test_instances[i,:] .= haralick_features( textures_test[i].patch )
      test_labels[i]=textures_test[i].class
   
end



model=svmtrain(instances', labels);

# Test model on the test data.
(predicted_labels, decision_values) = svmpredict(model, test_instances');

# Compute accuracy
println( "Accuracy:\t", mean((predicted_labels .== test_labels))*100)

