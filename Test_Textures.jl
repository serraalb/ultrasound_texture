using StaticArrays: ones
using Plots: xmin, xmax
using StatsBase
using Plots
using LinearAlgebra
using DelimitedFiles
using Images, ImageView
using MLJ
#using MLBase
include("utilities.jl")
include("wavelet.jl")
#include("scale.jl")

path="C://Users//HP//Desktop//alternativeproject//visionbook_Matlab_Code//15Texture//images_texture//"
#path="../images_texture/"

textures=[]
files=readdir(path)
 
patchsize = 100;
 
 for i = 1:length(files)
    img=Images.load(path*files[i])
    mask=ones(size(img));mask=Bool.(mask);
    img=Gray.(img);
    ny = size(img,1);
    nx = size(img,2);
   for ox = 1:patchsize-1:nx-patchsize+1
     for oy = 1:patchsize-1:ny-patchsize+1
         push!(textures,Texture(i, img[oy:oy+patchsize-1, ox:ox+patchsize-1 ], mask ) )
      end
   end
  
 end
n=length(textures)

#h=160# Haralick features
maxlevel=2; h=3*maxlevel+1 # Wavelet features

X=zeros(n, h);
y=zeros(n)

for i = 1:n
     
   # features= haralick_features( textures[i].patch )
   img=textures[i].patch; 
   mask=ones(size(img));
   mask=Bool.(mask);
   features=waveletdescr( img, mask, maxlevel)
    #sc=meanstd_scale(features')
    #X[i,:] .= ((sc.shift .+ features) .* sc.scale)
    X[i,:] .= features
    y[i]=textures[i].class
end

means= Statistics.mean(X,dims=1)
X = (X .- means) ./ Statistics.std(X,dims=1,mean=means)


X = MLJ.table(X)
y = categorical(y);

train, test = partition(eachindex(y), .8, rng=333);

SVM = @load SVC pkg=LIBSVM
model_svm = SVM(gamma=1.,cost=1.)

svc = machine(model_svm, X, y)
#MLJ.fit!(svc);
MLJ.fit!(svc, rows=train)
ypred = MLJ.predict(svc, rows=test)
classification_accuracy=(1-misclassification_rate(ypred, y[test]) )*100
ConfusionMatrix()(ypred, y[test])

# CrossValidation, mcr=mis-classification rate(mcr)
cv=CV(nfolds=5, shuffle=true)
#evaluate!(svc, resampling=cv,  measure=[rms, mcr], verbosity=0)
evaluate!(svc, resampling=cv,  measure=[mcr], verbosity=0)
