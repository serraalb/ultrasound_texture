using MLJ, MLJBase
import LIBSVM
using MLJBase: MLJModelInterface


cv=CV(nfolds=10, shuffle=true);
     
t= MLJBase.train_test_pairs(cv, 1:n);
train=t[1][1]; test=t[1][2];

svm = @load SVC pkg=LIBSVM 
r1= range(svm(), :gamma, lower=2^-15, upper=2^7);#lower=0.001, upper=1.0
r2 = range(svm(), :cost, lower=2^-4, upper=2^15);


stratified_cv = StratifiedCV(; nfolds=4, shuffle=true)
svc = TunedModel( model=svm(), measure=MisclassificationRate(),
                  operation=predict_mode,
                  tuning = Grid(resolution=20),
                  resampling=stratified_cv,                        
                  range=[r1, r2]);

w=ones(length(y))
w[y.==1] .= 2.0

svc_mach = machine(svc, X, y);
fit!(svc_mach)
report(svc_mach)[1]
svc=fitted_params(svc_mach).best_model





r1 = range(rfc(), :n_trees , lower=10, upper=50);
r2 = range(rfc(), :max_depth, lower=4, upper=100);
r3= range(rfc(), :min_samples_leaf, lower=5, upper=50);

rfc =@load RandomForestClassifier pkg=DecisionTree 
stratified_cv = StratifiedCV(; nfolds=4, shuffle=true)
# Tuning RF
rf_tuned = TunedModel(model=rfc(),  resampling=stratified_cv,                        
                      tuning = Grid(resolution=20), range=[r1, r2, r3]);

RF = machine( rf_tuned , X, y);
fit!(RF)




