using Distributions: minimum
import XLSX
using Distributions
using Statistics
using Plots
using StatsPlots
using DataFrames
import HypothesisTests
include("utilities.jl")
using Statistics, RDatasets, LIBLINEAR


#path="C://Users//HP//Desktop//patient_list.xlsx"
path="C://Users//kaushik//Desktop//old_desktop//patient_list.xlsx"
if !isfile(path)
    path="/mnt/medical/Ultrasound/majtan/patient_list.xlsx"
end
m = XLSX.readxlsx(path) # read the XLSX file
# columns, labels = XLSX.readtable(path, XLSX.sheetnames(m)[1])
sh=m[XLSX.sheetnames(m)[1]][:]  # first sheet as a table
df=DataFrame(XLSX.gettable(sh,header=true,infer_eltypes=true)) #

""" extract a column from a sheet by its name in the first row"""
function get_column_by_name(sheet,name)
   labels=sh[1,:]
   for i=1:length(labels)
       if  labels[i]==name
           return sheet[1:end,i]
       end
   end
   error("Column $name not found") 
end

# extract the first column with the diagnosis
dg=string.(get_column_by_name(sh,"Dg"))
controls= dg .== "KO" # extract bitmap for row indices for each class (33)
ehs= dg .== "EH"      # essential hypertension  (52)
pas= dg .== "PA"      # principal hyperaldosteronism (33)

# the interesting attributes
attrs=["BMI","Chol","HDL","LDL","Tg","Glyk","Kaz_STK","Kaz_DTK","Kaz_SF","D_STK","D_DTK","D_SF","N_STK","N_DTK","N_SF","24_STK","24_DTK","24_SF","PWV","CCAM","BULBM","MeanIMT","MaxIMT"]

#to_float(s) = parse(Float64,s)


function check_significance(sh,attr,class1=ehs,class2=pas)
    col=get_column_by_name(sh,attr)
    col1=convert(Vector{Float64},filter(!ismissing,col[class1]))
    col2=convert(Vector{Float64},filter(!ismissing,col[class2]))
    mean1=mean(col1)
    mean2=mean(col2)
    std1=std(col1)
    std2=std(col2)
    # perform Welch's test, KS test, MW U test
    welchp=HypothesisTests.pvalue(HypothesisTests.UnequalVarianceTTest(col1,col2),tail=:both)
    ksp=HypothesisTests.pvalue(HypothesisTests.ApproximateTwoSampleKSTest(col1,col2),tail=:both)
    mwp=HypothesisTests.pvalue(HypothesisTests.MannWhitneyUTest(col1,col2),tail=:both)
    # calculate the correlation coefficient
    x=vcat(col1,col2)
    y=vcat(ones(Float64,size(col1)),-ones(Float64,size(col2)))
    cc=cor(x,y)
    return ( name=attr, mean1=mean1, mean2=mean2, std1=std1, std2=std2,
             welchp=welchp, ksp=ksp, mwp=mwp, cc=cc )
end

Logging.disable_logging(Logging.Warn) # disable warning messages

for attr in attrs
    #if attr== "Glyk" || attr == "CCAM" || attr == "MeanIMT" || attr=="MaxIMT" 
      r=check_significance(sh,attr)
    if min(r.welchp,r.ksp,r.mwp)<0.05 # is it significant?
        println(r)
    end
end

# try classification with the three features - Glyk, 24_SF, CCAM
df=DataFrames.DataFrame()
df.glyk=get_column_by_name(sh,"Glyk")
#df.hf=get_column_by_name(sh,"24_SF") # heart frequency
df.avgimt=get_column_by_name(sh,"CCAM") # average IMT
#df.maximt=get_column_by_name(sh,"MaxIMT") # Max IMT 
#df.meanimt=get_column_by_name(sh,"MeanIMT") # Max IMT 
#df.dsf=get_column_by_name(sh,"D_SF") #  
#df.kazsf=get_column_by_name(sh,"Kaz_SF")  #"Kaz_SF"
#df.chol=get_column_by_name(sh,"Chol")  # Cholestrol
#df.hdl=get_column_by_name(sh,"HDL")  # HDL
#df.ldl=get_column_by_name(sh,"LDL")  # LDL
#df.bulbm=get_column_by_name(sh,"BULBM")  # BULBM


df.y= pas .- ehs
# drop incomplete cases and cases not in EH or PA
df=df[completecases(df),:]
df=df[df.y .!= 0, :]
#x=convert(Array{Float64},df[:,1:3])' # features
x=Matrix(df[:,1:3])'
x=convert(Array{Float64},x)
m1=LIBLINEAR.linear_train(df.y,x,solver_type=LIBLINEAR.L2R_LR,C=1.) # fit linear SVM
yt,ytprobs=LIBLINEAR.linear_predict(m1,x)
acc=mean(yt .== df.y)
@info "Linear SVM accuracy $acc"  # accuracy 73% (on training data)

# Box plots for differnt clinical parameters ("Glyk", "24_SF", "CCAM" , "MaxIMT" ) for EH and PA
attr=""
col=get_column_by_name(sh,attr)
col1=convert(Vector{Float64},filter(!ismissing,col[ehs]))
col2=convert(Vector{Float64},filter(!ismissing,col[pas]))
mindx= min(length(col1),length(col2) )
feature=[col1[1:mindx] col2[1:mindx] ]
p=boxplot([ "EH" "PH" ], feature , yaxis=:"Maximum IMT [mm]", xaxis =:"Classes", leg = false)
display(p)
savefig(p, "C://Users//kaushik//Desktop//Dr Holaj_Paper//box_plots//MaxIMT.pdf")

#=
attr="Glyk"
col=get_column_by_name(sh,attr)
col_ehs_glyk=convert(Vector{Float64},filter(!ismissing,col[ehs]))
col_pas_glyk=convert(Vector{Float64},filter(!ismissing,col[pas]))
attr="CCAM"
col=0
col=get_column_by_name(sh,attr)
col_ehs_ccam=convert(Vector{Float64},filter(!ismissing,col[ehs]))
col_pas_ccam=convert(Vector{Float64},filter(!ismissing,col[pas]))
col1=[col_pas_glyk; col_pas_ccam];col2=[col_ehs_glyk; col_ehs_ccam]
mindx= min(length(col1),length(col2) )
feature=[col1[1:mindx] col2[1:mindx] ]
p=boxplot([ "EH" "PA" ], feature , yaxis=:"CCAM IMT [mm] and Glucose [mmol/l]", xaxis =:"Classes", leg = false)
display(p)
savefig(p, "C://Users//kaushik//Desktop//Dr Holaj_Paper//box_plots//CCAM_Glucose.pdf")

=#

# N_controls=34;N_EH=50;N_PH=50
# fC=zeros(N, nf); fEH=zeros(N, nf); fPH=zeros(N, nf);

# data = hcat(columns...)
# # For combined
# bmi=[]; chol=[];hdl=[];ldl=[];tg=[];glyk=[];kaz_stk=[];kaz_dtk=[];
# kaz_sf=[]; d_stk=[]; d_dtk=[]; stk_24=[];dtk_24=[];pwv=[];ccam=[];
# bulbm=[];mean_imt=[];

# # For controls
# bmi_c=[]; chol_c=[];hdl_c=[];ldl_c=[];tg_c=[];glyk_c=[];kaz_stk_c=[];kaz_dtk_c=[];
# kaz_sf_c=[]; d_stk_c=[]; d_dtk_c=[]; stk_24_c=[];dtk_24_c=[];pwv_c=[];ccam_c=[];
# bulbm_c=[];mean_imt_c=[];

# # For EH
# bmi_eh=[]; chol_eh=[];hdl_eh=[];ldl_eh=[];tg_eh=[];glyk_eh=[];kaz_stk_eh=[];kaz_dtk_eh=[];
# kaz_sf_eh=[]; d_stk_eh=[]; d_dtk_eh=[]; stk_24_eh=[];dtk_24_eh=[];pwv_eh=[];ccam_eh=[];
# bulbm_eh=[];mean_imt_eh=[];

# # For PH
# bmi_ph=[]; chol_ph=[];hdl_ph=[];ldl_ph=[];tg_ph=[];glyk_ph=[];kaz_stk_ph=[];kaz_dtk_ph=[];
# kaz_sf_ph=[]; d_stk_ph=[]; d_dtk_ph=[]; stk_24_ph=[];dtk_24_ph=[];pwv_ph=[];ccam_ph=[];
# bulbm_ph=[];mean_imt_ph=[];


# nc=1:33
# neh=35:85;
# nph=86:118
# #= keep = map(!ismissing, chol_eh[1])
# chol_eh[1][keep]
# size(chol_eh[1][keep],1)
#  =#
#  # take the colum entries of interest
# for l =1: length(labels)
    
#     # Body mass Index
#     if (labels[l] == :BMI)

#         push!(bmi_c, filter((!ismissing),data[nc,l]) )
#         push!(bmi_eh, filter((!ismissing),data[neh,l]) )
#         push!(bmi_ph, filter((!ismissing),data[nph,l]) )
#         push!(bmi, filter((!ismissing),data[:,l]) )
#         # Cholestrol
#     elseif (labels[l]== :Chol )
#         push!(chol_c, filter((!ismissing),data[nc,l]))
#         push!(chol_eh, filter((!ismissing),data[neh,l]))
#         push!(chol_ph, filter((!ismissing),data[nph,l]))
#         push!(chol, filter((!ismissing),data[:,l]) )

#     elseif (labels[l]== :HDL )
#         push!(hdl_c, filter((!ismissing),data[nc,l]))
#         push!(hdl_eh, filter((!ismissing),data[neh,l]))
#         push!(hdl_ph, filter((!ismissing),data[nph,l]))
#         push!(hdl, filter((!ismissing),data[:,l]) )
    
#     elseif (labels[l]== :LDL )
#         push!(ldl_c, filter((!ismissing),data[nc,l]))
#         push!(ldl_eh, filter((!ismissing),data[neh,l]))
#         push!(ldl_ph, filter((!ismissing),data[nph,l]))
#         push!(ldl, filter((!ismissing),data[:,l]) )
   
#     elseif (labels[l]== :Tg )
#         push!(tg_c, filter((!ismissing),data[nc,l]))
#         push!(tg_eh, filter((!ismissing),data[neh,l]))
#         push!(tg_ph, filter((!ismissing),data[nph,l]))
#         push!(tg, filter((!ismissing),data[:,l]) )
      
#     elseif (labels[l]== :Glyk )
#         push!(glyk_c, filter((!ismissing),data[nc,l]))
#         push!(glyk_eh, filter((!ismissing),data[neh,l]))
#         push!(glyk_ph, filter((!ismissing),data[nph,l]))
#         push!(glyk, filter((!ismissing),data[:,l]) )


#     elseif (labels[l]== :Kaz_SF )
#         push!(kaz_sf_c, filter((!ismissing),data[nc,l]))
#         push!(kaz_sf_eh, filter((!ismissing),data[neh,l]))
#         push!(kaz_sf_ph, filter((!ismissing),data[nph,l]))
#         push!(kaz_sf, filter((!ismissing),data[:,l]) )

#     elseif (labels[l]== :D_STK )
#         push!(d_stk_c,filter((!ismissing),data[nc,l]))
#         push!(d_stk_eh, filter((!ismissing),data[neh,l]))
#         push!(d_stk_ph, filter((!ismissing),data[nph,l]))
#         push!(d_stk, filter((!ismissing),data[:,l]) )
        
#     elseif (labels[l]== :D_DTK )
        
#         push!(d_dtk_c, filter((!ismissing),data[nc,l]))
#         push!(d_dtk_eh, filter((!ismissing),data[neh,l]))
#         push!(d_dtk_ph, filter((!ismissing),data[nph,l]))
#         push!(d_dtk, filter((!ismissing),data[:,l]) )
             
#     elseif (l== 43 )
        
#         push!(stk_24_c, filter((!ismissing),data[nc,l])) 
#         push!(stk_24_eh, filter((!ismissing),data[neh,l]))
#         push!(stk_24_ph, filter((!ismissing),data[nph,l]))
#         push!(stk_24, filter((!ismissing),data[:,l]) )
    
#     elseif (l== 44)
        
#         push!(dtk_24_c, filter((!ismissing),data[nc,l]))
#         push!(dtk_24_eh, filter((!ismissing),data[neh,l]))
#         push!(dtk_24_ph, filter((!ismissing),data[nph,l]))
#         push!(dtk_24, filter((!ismissing),data[:,l]) )
 
#     elseif (labels[l]== :PWV )
#         push!(pwv_c, filter((!ismissing),data[nc,l]))
#         push!(pwv_eh, filter((!ismissing),data[neh,l]))
#         push!(pwv_ph, filter((!ismissing),data[nph,l]))
#         push!(pwv, filter((!ismissing),data[:,l]) )
   
#     elseif (labels[l]== :CCAM )
        
#         push!(ccam_c, filter((!ismissing),data[nc,l]))
#         push!(ccam_eh, filter((!ismissing),data[neh,l]))
#         push!(ccam_ph, filter((!ismissing),data[nph,l]))
#         push!(ccam, filter((!ismissing),data[:,l]) )

#     elseif (labels[l]== :BULBM )
#         push!(bulbm_c, filter((!ismissing),data[nc,l]))
#         push!(bulbm_eh, filter((!ismissing),data[neh,l]))
#         push!(bulbm_ph, filter((!ismissing),data[nph,l]))
#         push!(bulbm, filter((!ismissing),data[:,l]) )
    
#     elseif (labels[l]== :MeanIMT )
        
#         push!(mean_imt_c, filter((!ismissing),data[nc,l]))
#         push!(mean_imt_eh, filter((!ismissing),data[neh,l]))
#         push!(mean_imt_ph, filter((!ismissing),data[nph,l]))
#         push!(mean_imt, filter((!ismissing),data[:,l]) )

#     end

# end


# #= length(mean_imt_ph[1])
# t_C_EH, p_C_EH =TwoSampleT2Test(fC[:,1:nf] ,fEH[:,1:nf]);
#  =#
#  ## Box Plots for various parameters for C, EH and PH
#  fc=mean_imt_c
#  feh=mean_imt_eh
#  fph=mean_imt_ph
# ["C" mean(fc[1]) std(fc[1])]
# ["EH"  mean(feh[1]) std(feh[1])]
# ["PH"  mean(fph[1]) std(fph[1])]


# num=30
# feature=[  fc[1][1:num]  feh[1][1:num]   fph[1][1:num] ]
# p=boxplot(["Controls" "EH" "PH" ], feature , label=false)
# #savefig(p, "C:\\Users\\HP\\Desktop\\HyperTensionGit\\paper\\f_rf\\bp_bmi.png")

# ## p-values for various parameters for C-PH and EH-PH pairs

# fc=mean_imt_c
# feh=mean_imt_eh
# fph=mean_imt_ph

# nf=min(size(fc[1],1), size(feh[1],1), size(fph[1],1))

# fC=zeros(nf,1);fEH=zeros(nf,1);fPH=zeros(nf,1);
# fC .= fc[1][1:nf]
# fEH .= feh[1][1:nf]
# fPH .= fph[1][1:nf]

# t_c_ph, p_c_ph =TwoSampleT2Test(fC[1:nf, 1:1] ,fPH[1:nf, 1:1])
# t_eh_ph, p_eh_ph =TwoSampleT2Test(fEH[1:nf, 1:1] ,fPH[1:nf, 1:1])

# """
# This code is for correlation among 14 clinical parameters
# """
# # keeping common size, p =90
# p=90
# v=zeros(Float64, 8, p)

# #v[1,1:p] .= bmi[1][1:p] .+ 0.
# v[2,1:p] .= chol[1][1:p] .+ 0.
# #v[3,1:p] .= hdl[1][1:p] .+ 0.
# #v[4,1:p] .= ldl[1][1:p] .+ 0.

# v[1,1:p] .= tg[1][1:p] .+ 0.
# #v[6,1:p] .= glyk[1][1:p] .+ 0.
# #v[7,1:p] .= kaz_sf[1][1:p] .+ 0.
# v[4,1:p] .= d_stk[1][1:p] .+ 0.
# v[3,1:p] .= d_dtk[1][1:p] .+ 0.

# v[5,1:p] .= stk_24[1][1:p] .+ 0.
# v[6,1:p] .= dtk_24[1][1:p] .+ 0.
# #v[12,1:p] .= pwv[1][1:p] .+ 0.
# v[8,1:p] .= ccam[1][1:p] .+ 0.
# #v[14,1:p] .= bulbm[1][1:p] .+ 0.
# v[7,1:p] .= mean_imt[1][1:p] .+ 0.

# cor(v')





